<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] DJ Page.
 *
 *  [AP] DJ Page is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  [AP] DJ Page is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with [AP] DJ Page.  If not, see <https://www.gnu.org/licenses/>.
 */ 

namespace apathy\DjPage\Admin\Controller;

use apathy\DjPage\Entity\Mix as Entity;
use XF;
use XF\Mvc\ParameterBag;
use XF\Admin\Controller\AbstractController;

class Mix extends AbstractController
{
    public function actionIndex(ParameterBag $params)
    {
        if($params->mix_id)
        {
            return $this->rerouteController(__CLASS__, 'viewMix', $params);
        }

        /** @var \apathy\DjPage\Repository\Mix $repo */
        $repo = $this->repository('apathy\DjPage:Mix');

        $viewParams = [
            'mixes' => $repo->findActiveMixesForList()
        ];

        return $this->view('apathy\DjPage:Mix', 'ap_dj_view_mix_list', $viewParams);
    }

    public function actionViewMix(ParameterBag $params)
    {
        $mix = $this->assertViewableMIx($params['mix_id']);

        $viewParams = [
            'mix' => $mix
        ];

        return $this->view('apathy\DjPage:Mix', 'ap_dj_view_mix_overview', $viewParams);
    }

    public function actionAddEdit(Entity $mix)
    {
        $viewParams = [
            'mix' => $mix
        ];

        return $this->view('apathy\DjPage:Mix\Edit', 'ap_dj_mix_edit', $viewParams);
    }

    public function actionAdd()
    {
        $type = $this->filter('type', 'str');

        /** @var \apathy\DailyGoal\Repository\Goal $repo */
        $repo = $this->repository('apathy\DailyGoal:Goal');

        if($type)
        {
            $goal = $repo->findGoalOfType($type);

            if($goal)
            {
                return $this->error(XF::phrase('ap_dg_this_goal_already_exists'));
            }

            $handler = $repo->getHandler($type);
            $goal = $this->initializeNewMix($type, $handler->getTitle()->render());

            return $this->actionAddEdit($goal);
        }

        $viewParams = [
            'type'      => $type,
            'goalTypes' => $repo->getGoalTypes()
        ];

        return $this->view('apathy\DailyGoal:Goal\Chooser', 'ap_dg_goal_chooser', $viewParams);
    }

    public function actionDelete(ParameterBag $params)
    {
        $mix = $this->assertViewableMix($params->mix_id);

        /** @var \XF\ControllerPlugin\Delete $plugin */
        $plugin = $this->plugin('XF:Delete');

        return $plugin->actionDelete(
            $mix,
            $this->buildLink('apathy-dj/mixes/delete', $mix),
            $this->buildLink('apathy-dj/mixes/edit', $mix),
            $this->buildLink('apathy-dj/mixes'),
            $mix->title
        );
    }

    public function actionEdit(ParameterBag $params)
    {
        $mix = $this->assertViewableMix($params->mix_id);

        return $this->actionAddEdit($mix);
    }

    public function actionSave(ParameterBag $params)
    {
        $this->assertPostOnly();

        if($params->mix_id)
        {
            $mix = $this->assertViewableMix($params->mix_id);
        }
        else
        {
            $mix = $this->app()->em()->create('apathy\DjPage:Mix');
        }

        $this->mixSaveProcess($mix)->run();

        return $this->redirect($this->buildLink('apathy-dj/mixes/edit', $mix));
    }

    public function actionToggle()
    {
        $plugin = $this->plugin('XF:Toggle');

        return $plugin->actionToggle('apathy\DjPage:Mix');
    }

    protected function initializeNewMix(string $title)
    {
        $mix = $this->app()->em()->create('apathy\DjPage:Mix');

        $mix->title = $title;

        return $mix;
    }

    protected function mixSaveProcess(Entity $mix)
    {
        $form = $this->formAction();

        $input = $this->filter([
            'description'  => 'str',
            'release_date' => 'uint',
            'genre_list'   => 'array',
            'tracklist'    => 'array'
        ]);

        $form->basicEntitySave($mix, $input);

        return $form;
    }

    protected function assertViewableMix($mixId, $with = NULL)
    {
        return $this->assertViewableRecord('apathy\DjPage:Mix', $mixId, $with);
    }
}