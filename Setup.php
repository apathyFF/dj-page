<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] DJ Page.
 *
 *  [AP] DJ Page is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  [AP] DJ Page is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with [AP] DJ Page.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DjPage;

use XF\AddOn\AbstractSetup;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
	public function install(array $stepParams = [])
	{
		$schemaManager = $this->schemamanager();

        $schemaManager->createTable('xf_ap_dj_mix', function(Create $table)
        {
            $table->addColumn('mix_id', 'int')->autoIncrement();
            $table->addPrimaryKey('mix_id');

            $table->addColumn('title', 'varchar', 200);
            $table->addColumn('description', 'text');
            $table->addColumn('release_date', 'int');
            $table->addColumn('genre_list', 'blob');
            $table->addColumn('tracklist', 'blob');
            $table->addColumn('play_count', 'int')->setDefault(0);
            $table->addColumn('mix_state', 'enum')->values(['visible','private','deleted'])->setDefault('visible');
        });
	}

	public function upgrade(array $stepParams = [])
	{
		// TODO: Implement upgrade() method.
	}

	public function uninstall(array $stepParams = [])
	{
		$this->schemaManager()->dropTable('xf_ap_dj_mix');
	}
}