<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] DJ Page.
 *
 *  [AP] DJ Page is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  [AP] DJ Page is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with [AP] DJ Page.  If not, see <https://www.gnu.org/licenses/>.
 */ 

namespace apathy\DjPage\Pub\Controller;

use XF;
use XF\Mvc\ParameterBag;
use XF\Pub\Controller\AbstractController;

class Mix extends AbstractController
{
    public function actionIndex(ParameterBag $params)
    {
        if($params->mix_id)
        {
            return $this->rerouteController(__CLASS__, 'viewMix', $params);
        }

        /** @var \apathy\DjPage\Repository\Mix $repo */
        $repo = $this->repository('apathy\DjPage:Mix');

        $viewParams = [
            'mixes' => $repo->findActiveMixesForList()
        ];

        return $this->view('apathy\DjPage:Mix', 'ap_dj_view_mix_list', $viewParams);
    }

    public function actionViewMix(ParameterBag $params)
    {
        $mix = $this->assertViewableMIx($params['mix_id']);

        $viewParams = [
            'mix' => $mix
        ];

        return $this->view('apathy\DjPage:Mix', 'ap_dj_view_mix_overview', $viewParams);
    }

    protected function assertViewableMix($mixId, $with = NULL)
    {
        return $this->assertViewableRecord('apathy\DjPage:Mix', $mixId, $with);
    }

    public static function getActivityDetails(array $activities)
    {
        $app = XF::app();
        $router = $app->router('public');
        $defaultPhrase = XF::phrase('ap_dj_view_mix');

        $output = [];

        foreach($activities AS $key => $activity)
        {
            $mixId = $activity['params'];

            $mix = $app->em()->find('apathy\DjPage:Mix', $mixId);

            if(!empty($mix))
            {
                $url = $router->buildLink('apathy-dj/mixes', [
                    'title'  => $mix->title,
                    'mix_id' => $mix->mix_id
                ]);

                $output[$key] = [
                    'description' => XF::phrase('ap_dj_viewing_mix_x'),
                    'title'       => $mix->title,
                    'url'         => $url
                ];
            }
            else
            {
                $output[$key] = $defaultPhrase;
            }
        }

        return $output;
    }
}