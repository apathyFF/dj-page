<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] DJ Page.
 *
 *  [AP] DJ Page is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  [AP] DJ Page is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with [AP] DJ Page.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DjPage\Entity;

use XF;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 * @property int    $mix_id
 * @property string $title
 * @property string $description
 * @property int    $release_date
 * @property array  $genre_list
 * @property array  $tracklist
 * @property int    $play_count
 * @property string $mix_state
 * @property bool   $active
 * @property int    $display_order
 * @property int    $cover_date
 */

class Mix extends Entity
{
    public function getAbstractedCoverPath($size): string
	{
        return sprintf('data://apathy/DjPage/covers/%s/%s.jpg', $size, $this->mix_id);
	}

	public function getCoverUrl($sizeCode, $canonical = false)
	{
		$app = $this->app();

        $sizeMap = $this->getSizeMap();

		if(!isset($sizeMap[$sizeCode]))
		{
			$sizeCode = 'l';
		}

		return $app->applyExternalDataUrl(
			"apathy/DjPage/covers/{$sizeCode}/{$this->mix_id}.jpg?{$this->cover_date}",
			$canonical
		);
	}

    public function getSizeMap(): array
    {
        return ['l' => 200, 'm' => 100, 's' => 50];
    }

    public static function getStructure(Structure $structure): Structure
    {
        $structure->table = 'xf_ap_dj_mix';
        $structure->shortName = 'apathy\DjPage:Mix';
        $structure->contentType = 'dj_mix';
        $structure->primaryKey = 'mix_id';
        $structure->columns = [
            'mix_id' => [
                'type'          => self::UINT, 
                'autoIncrement' => true
            ],
            'title' => [
                'type'          => self::STR,
                'maxLength'     => 200
            ],
            'description' => [
                'type'          => self::STR,
                'default'       => ''
            ],
            'release_date' => [
                'type'          => self::UINT, 
                'default'       => XF::$time
            ],
            'genre_list' => [
                'type'          => self::LIST_COMMA, 
                'default'       => [],
				'list'          => [
                    'type'   => 'posint', 
                    'unique' => true
                ]
            ],
            'tracklist' => [
                'type'          => self::JSON_ARRAY,
                'default'       => []
            ],
            'play_count' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'mix_state' => [
                'type'          => self::STR, 
                'default'       => 'visible',
                'allowedValues' => [
                    'visible', 
                    'private', 
                    'deleted'
                ]
            ],
            'active' => [
                'type'          => self::BOOL,
                'default'       => 1
            ],
            'display_order' => [
                'type'          => self::UINT, 
                'default'       => 1
            ],
            'cover_date' => [
                'type'          => self::UINT,
                'default'       => XF::$time
            ]
        ];
        $structure->relations = [];
        $structure->defaultWith = [];
        $structure->getters = [];
        $structure->behaviors = [];

        return $structure;
    }

    public function canView(&$error = null)
    {
        return true;
    }
}